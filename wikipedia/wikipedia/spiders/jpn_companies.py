import re
import sys
#import itertools
from pathlib import Path
from urllib.parse import unquote


import scrapy

#spider_dir = Path(__file__).absolute()
#sys.path.append(spider_dir.parent.parent.parent.parent)
#breakpoint()
##from utils import strip_tags
#from ..utils import strip_tags

from html.parser import HTMLParser
from io import StringIO


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def rm_inner(html, tag="ul"):
    """
    args
        html, str
            the part of HTML str of which you'd like to remove some inner part

    return
    """
    start = f"<{tag}>"
    start_idx = html.find(start)
    if start_idx == -1:
        return html

    end = f"</{tag}>"
    end_idx = html.rfind(end)
    return html[:start_idx] + html[end_idx+len(end):]


# TODO: decide rules to remove
# 1. rm everything after first "("
# 2. rm () and []
# 3. <dl> as child of <li>
# 4.  {"小売業": ["小売業に分類される上場および未上場の主要企業を収録。上場企業は証券コードを加えている。", "他業種の一覧と合わせるため、順序は業種にかかわらず社名の50音順。「株式会社」は省略。", "安楽亭【焼肉ファミリーレストラン】", "味の民芸フードサービス【和食レストラン「味の民芸」】"}
# 5. 旧: e.g.（旧昭和ゴム）
# 6. 陸運 -> 北海道の乗合バス事業者
# 7. 情報・通信 -> 日本の出版社一覧
# 8. サービス: split by arrow -> regex rm everything after opening parentheses
#itertools.chain.from_iterable()
#greedy_parentheses = re.compile(r'（.*）')
#greedy_parentheses = re.compile(r'（.*）\[?[0-9]?\]?')
#greedy_parentheses = re.compile(r'[（(].*[）)]')
#greedy_parentheses = re.compile(r'[（(].*[）)]\[?[0-9]?\]?')  # good
#greedy_parentheses = re.compile(r'[（(].*[）)]\[?[0-9]?\]?【?.*】?')
#greedy_parentheses = re.compile(r'[（(\[【].*[）)\]】]')      # good
#greedy_parentheses = re.compile(r'[（(\[【《].*[）)\]】》]')
greedy_parentheses = re.compile(r'[（(\[【《].*$')
def extract_company_name(ul, keep_all_arrow=True):
    unnested = ul.xpath('./li[not(./ul)]')
    nested = ul.xpath('./li[./ul]')

    arrow = "→"
    unnested_companies = []
    #greedy_parentheses.sub("", strip_tags(s)).strip() for s in unnested.getall()
    for s in unnested.getall():
        if "。" in s:
            continue
        #intermediate = greedy_parentheses.sub("", strip_tags(s)).strip()
        intermediate = strip_tags(s).strip()
        if arrow in intermediate:
            if keep_all_arrow:
                unnested_companies.extend(
                    [greedy_parentheses.sub("", s).strip()
                     for s in intermediate.split(arrow)]
                )
            else:
                # We take only the rightmost company name in this case
                unnested_companies.append(
                    greedy_parentheses.sub(
                        "",
                        intermediate.split(arrow)[-1]
                    ).strip()
                )
        else:
            unnested_companies.append(
                greedy_parentheses.sub("", intermediate).strip()
            )

    # TODO: nested_companies
    nested_companies = []
    for li in nested:
        s = rm_inner(li.get())
        if "。" not in s:
            intermediate = greedy_parentheses.sub("", strip_tags(s)).strip()
            if arrow in intermediate:
                if keep_all_arrow:
                    nested_companies.extend(
                        [s.strip() for s in intermediate.split(arrow)]
                    )
                else:
                    # We take only the rightmost company name in this case
                    nested_companies.append(
                        intermediate.split(arrow)[-1].strip()
                    )
            else:
                nested_companies.append(intermediate)

        inner_ul = nested.xpath('./ul')
        nested_companies.extend(
            extract_company_name(inner_ul, keep_all_arrow=keep_all_arrow))

    companies = unnested_companies
    companies.extend(nested_companies)

    return companies


class JpnCompaniesSpider(scrapy.Spider):
    name = "jpn_companies"
    allowed_domains = ["ja.wikipedia.org"]
    mother_word = "%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7"
    #forbid_url_word = "%81%AE%E4%B8%80%E8%A6%A7" # i.e. 一覧
    forbid_url_word = "%A5%AD%E4%B8%80%E8%A6%A7" # i.e. 一覧
    start_urls = [
        #"http://ja.wikipedia.org/",
        #"https://ja.wikipedia.org/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7",
        f"https://ja.wikipedia.org/wiki/{mother_word}",
    ]

    def parse(self, response):
        # Note that // in xpath is similar to /**/ in Linux command line
        subcategory_anchors = response.xpath('//table[@class="wikitable"]//tr/td/a')
        subcategory_hrefs = [a.attrib["href"] for a in subcategory_anchors]
        # The hrefs are percent-encoded; one could easily decode them by
        # unquote(href), where href is a percent-encoded string and
        # (from urllib.parse import unquote)

        yield from response.follow_all(subcategory_hrefs, self.parse_subcategory)

    def parse_subcategory(self, response):
        decoded = unquote(response.url)
        # Example decoded be like
        # "https://ja.wikipedia.org/wiki/日本の企業一覧_(証券・商品先物取引)"
        index = decoded.find("(")
        if index != -1:
            subcategory = decoded[index+1:-1]
        else:
            subcategory = decoded[decoded.find("日本の")+3: decoded.find("一覧")]

        # TODO: Is skipping 銀行 this way appropriate?
        if subcategory == "銀行":
            return

        #items = response.xpath('//li').getall()
        #items = response.xpath('//li')
        #items = response.xpath('//li[not(contains(@id, "pt-createaccount-2"))]').getall()
        #items = response.xpath('//li[not(contains(@id, "pt-createaccount-2"))]/text()').getall()
        #items = response.xpath('//li[not(contains(@id, "pt-createaccount-2"))]').getall()
        #items = [strip_tags(html) for html in response.xpath('//li[not(@id)]').getall()]

        #items = response.xpath('//li[not(@id)]')
        ## Still some unwanted
        #wanted = for i in items if i.xpath('a')
        #items = response.xpath('//li[not(@id)]/a[not(contains(@title, "Category"))]')
        #items = [strip_tags(html) for html in response.xpath('//li[not(@id)]/a[not(contains(@title, "Category"))]').getall()]

        # TODO: Work fine. keep or discard?
        #items = [strip_tags(html) for html in response.xpath(f'//li[not(@id)]/a[not(contains(@title, "Category")) and not(contains(@href, "{JpnCompaniesSpider.mother_word}"))]').getall()]

        ## TODO: Above too restrictive.
        ##items = response.xpath(f'//li[not(@id)]')
        #items = response.xpath(f'//div[@class="mw-parser-output"]/ul/li[not(@id)]')
        #companies = [i for i in items
        #    if (i.xpath('not(a)') and "。" not in i.get())
        #    or i.xpath(f'a[not(contains(@title, "Category")) and not(contains(@href, "{JpnCompaniesSpider.mother_word}"))]')
        #]
        #cs = [c.xpath('string()').get() for c in companies]
        ##cs = [c.xpath('string(//li)').get() for c in companies]


        #companies = [i for i in items if i.xpath('not(a)') or i.xpath(f'//a[not(contains(@title, "Category")) and not(contains(@href, "{JpnCompaniesSpider.forbid_url_word}"))]')]

        # TODO: preceding-sibling::
        """
        In [279]: len(response.xpath(f'//ul[preceding-sibling::h2[span/@id="関連項目"]]'))
        Out[279]: 1

        In [305]: len(response.xpath(f'//ul[preceding-sibling::h2[span[@class="mw-headline"]/@id="関連項目"]]'))
        Out[305]: 1
       
        In [307]: len(response.xpath(f'//ul[preceding-sibling::h2[not(span[@class="mw-headline"]/@id="関連項目")]]'))
        Out[307]: 137

        In [306]: len(response.xpath(f'//ul[not(preceding-sibling::h2[span[@class="mw-headline"]/@id="関連項目"])]'))
        Out[306]: 238

        In [358]: len(response.xpath('//ul[preceding-sibling::h2[span[@class="mw-headline"]] and not(preceding-sibling::h2[span[@id="関連項目"]])]'))
        Out[358]: 136

        In [369]: (response.xpath('//h2[not(span[@id="関連項目"])]/following-sibling::ul'))[-1].get()
        Out[369]: '<ul><li><a href="/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7" title="日本の企業一
          ">日本の企業一覧</a></li>\n<li><a href="/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7_(%E5%8D%B8%E5%A3%B2%E6%A5%AD)" title="日本の企業一覧 (卸売業)">日本の企業一覧 (卸売業)</a></li></ul>'
        
        In [370]: (response.xpath('//h2[not(span[@id="関連項目"])]'))[-1].get()
        Out[370]: '<h2><span id=".E9.A3.9F.E6.9D.90.E5.AE.85.E9.85.8D.E3.82.B5.E3.83.BC.E3.83.93.E3.82.B9"></span><span class="mw-headline" id="食材宅配サービス">食材宅配サービス</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7_(%E5%B0%8F%E5%A3%B2%E6%A5%AD)&amp;action=edit&amp;section=53" title="節を編集: 食材宅配サービス">編集</a><span class="mw-editsection-bracket">]</span></span></h2>'
        """
        #response.xpath(f'//ul/preceding-sibling::h2/span[@id!="関連項目"]')
        #response.xpath(f'//ul[preceding-sibling::h2[./span[@id="関連項目"]]]')
        #response.xpath(f'//ul[preceding-sibling::h2[not(./span/@id="関連項目")]]')
        #response.xpath('//ul[preceding-sibling::h2[span[@class="mw-headline"]] and not(preceding-sibling::h2[span[@id="関連項目"]])]')
        #outmost_uls = response.xpath('//h2[not(span[@id="関連項目"])]/following-sibling::ul[1]')



        # TODO: h2 or h3
        #response.xpath('(//h2) | (//h3)')
        #h2_uls = response.xpath('//ul[preceding-sibling::*[1][name()="h2" and ./span[@class="mw-headline"]/text()!="関連項目"]]')
        #h2_uls = response.xpath('//ul[preceding-sibling::*[1][name()="h2" and ./span[@class="mw-headline"]/text()!="関連項目" and text()!="外部リンク"]]')
        h2_uls = response.xpath('//ul[preceding-sibling::*[1][name()="h2" and ./span[@class="mw-headline"][./text()!="関連項目" and ./text()!="外部リンク"]]]')
        h3_uls = response.xpath('//ul[preceding-sibling::*[1][name()="h3"]]')
        h4_uls = response.xpath('//ul[preceding-sibling::*[1][name()="h4"]]')
        p_uls = response.xpath('//ul[preceding-sibling::*[1][name()="p"]]')
        dl_uls = response.xpath('//ul[preceding-sibling::*[1][name()="dl"]]')

        n_uls = len(h2_uls) + len(h3_uls) + len(h4_uls) + len(p_uls) + len(dl_uls)

        outmost_uls = h2_uls
        outmost_uls.extend(h3_uls)
        outmost_uls.extend(h4_uls)
        outmost_uls.extend(p_uls)
        outmost_uls.extend(dl_uls)
        companies = []
        for ul in outmost_uls:
            companies.extend(extract_company_name(ul))


        # Unwanted h2:
        # 関連項目
        # 外部リンク <https://ja.wikipedia.org/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7_(%E9%9B%BB%E6%B0%97%E3%83%BB%E3%82%AC%E3%82%B9)>
        # 関連団体 <https://ja.wikipedia.org/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E4%BC%81%E6%A5%AD%E4%B8%80%E8%A6%A7_(%E3%83%91%E3%83%AB%E3%83%97%E3%83%BB%E7%B4%99)>


        ## TODO: both h2 and ul
        #selectors = response.xpath(f'//div[@class="mw-parser-output"]/*[self::h2 | self::ul/li[not(@id)]]')
        #unwanted_h2 = "関連項目"
        ##s[-2].xpath('span[@id="関連項目"]/text()').get()
        #h2_indices = []
        #uls = []
        #is_unwanted = False
        ##is_関連項目 = False
        #for j, s in enumerate(selectors):
        #    if s.root.tag == "h2":
        #        is_unwanted = s.xpath('span[@id="関連項目"]/text()').get()
        #        continue
        #    if is_unwanted:
        #        continue
        #    uls.append(s)

        #upper_lis = []
        #for j, ul in enumerate(uls):
        #    upper_lis.extend(ul.xpath('li'))



        #items = response.xpath(f'//li[not(@id)]/a[not(contains(@title, "Category"))]')
        #items = response.xpath(f'//li[not(@id)]/a[not(contains(@title, "Category"))]]').extend()
        # TODO:
        # 1. make sure those w/o anchor are included
        # 2. make sure non-issuer are excluded
        #items = response.xpath(f'//li[not(@id)]')
        #no_anchor = [i for i in items if i.xpath('//not(a)')]
        #yes_anchor = [i for i in items if i.xpath('//a[not(contains(@title, "Category"))]')]
        #items = no_anchor.extend(yes_anchor)
        yield {
            #subcategory: items,
            #subcategory: n_uls
            subcategory: companies
        }
