import argparse
import json
from pathlib import Path


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("jsonline", help="path to jsonline file")
    args = parser.parse_args()

    D = {}
    S = set()
    in_path = Path(args.jsonline)
    if in_path.is_file():
        with open(args.jsonline, "r") as f:
            for line in f:
                D.update(json.loads(line))

        n_companies = 0
        for k, v in D.items():
            print(k, len(v))
            S.update(v)

        #print(f"n_companies = {len(S)}")

        out_path = f"{in_path.stem}.tsv"
        with open(out_path, "w") as f:
            f.write("\t".join(S))
