import argparse
import ipdb
import json

def get_companies(json_path):
    D = {}
    with open(json_path, "r") as f:
        for line in f:
            D.update(json.loads(line))
    for k, v in D.items():
        D[k] = set(v)
    return D


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "jsonline",
        nargs=2,
        help="paths to 2 jsonline files",
    )
    args = parser.parse_args()
    #print(f"{args = }")
    #print(f"{args.jsonline = }")
    path_L, path_R = args.jsonline
    L = get_companies(path_L)
    R = get_companies(path_R)
    #ipdb.set_trace()
    assert set(L.keys()) == set(R.keys()), "L, R have diff keys!"
    for k in L:
        L_more = L[k] - R[k]
        R_more = R[k] - L[k]
        print(f"{k}")
        if L_more:
            print(f"\tL - R = {L_more}")
        if R_more:
            print(f"\tR - L = {R_more}")
        if not L_more and not R_more:
            print(f"\tL == R")
        print()
