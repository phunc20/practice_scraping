## Simple Statistics
```python
In [1]: import json

In [2]: D = {}

In [3]: with open("out.jsonl", "r") as f:
   ...:     for line in f:
   ...:         D.update(json.loads(line))

In [4]: for k, v in D.items():
   ...:     print(f"{k}: {len(v)}")
   ...:
水産・農林: 18
鉱業: 26
パルプ・紙: 79
石油・石炭製品: 43
情報・通信: 1898
繊維製品: 150
化学: 281
医薬品: 277
倉庫・運輸関連: 81
食料品: 697
海運: 57
陸運: 289
建設: 579
空運: 59
電気・ガス: 522
その他製品: 401
不動産: 518
銀行: 360
保険: 88
その他金融: 138
証券・商品先物取引: 220
輸送用機器: 157
小売業: 1116
電気機器: 726
卸売業: 524
精密機器: 94
機械: 320
金属製品: 119
サービス: 4876
非鉄金属: 57
ガラス・土石製品: 95
鉄鋼: 66
ゴム製品: 40
```


https://en.wikipedia.org/wiki/XPath
https://www.w3.org/TR/xpath/
https://www.w3.org/TR/1999/REC-xpath-19991116/
https://www.scrapingbee.com/blog/practical-xpath-for-web-scraping/
