import argparse
import json


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("jsonline", help="path to jsonline file")
    args = parser.parse_args()

    D = {}
    S = set()
    with open(args.jsonline, "r") as f:
        for line in f:
            D.update(json.loads(line))
    n_companies = 0
    for k, v in D.items():
        print(k, len(v))
        S.update(v)
    print(f"n_companies = {len(S)}")
