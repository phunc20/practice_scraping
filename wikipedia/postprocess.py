import argparse
import json
import re
from pathlib import Path


def end_by(text, pattern):
    return text[-len(pattern):] == pattern


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "jsonline",
        help="paths to a jsonline file",
    )
    args = parser.parse_args()
    json_path = Path(args.jsonline)
    D = {}
    with open(json_path, "r") as f:
        for line in f:
            D.update(json.loads(line))

    fw_close_paren = "）"
    #year_pattern = re.compile(r'[0-9０-９]+年')
    year_pattern = re.compile(r'\d+年')
    for cat, L in D.items():
        new_L = []
        for name in L:
            unwanted_inpattern = False
            for pattern in (
                "#",
                "一覧",
                "年間",
                "百年住宅",
            ):
                if pattern in name:
                    unwanted_inpattern = True

            if unwanted_inpattern:
                continue

            #if "年" in name:
            #    print(f"{cat}: {name}")

            year_match = year_pattern.search(name)
            if year_match:
                continue

            for pattern in ("事業者", "放送局"):
                if end_by(name, pattern):
                    continue

            if fw_close_paren in name:
                if name[-1] == fw_close_paren:
                    new_L.append(name[:-1])
                continue

            new_L.append(name)
        D[cat] = new_L

    #print(f"{D}")
    #with open("final.json", "w") as f:
    #    json.dump(D, f, ensure_ascii=False)

    with open("final.jsonlines", "w") as f:
        for k, v in D.items():
            line = json.dumps({k: v}, ensure_ascii=False)
            f.write(line+"\n")
