import scrapy
from scrapy_playwright.page import PageMethod

class VnexpressCommentPlaywrightSpider(scrapy.Spider):
    name = 'vnexpress_comment_playwright'
    #allowed_domains = ['test.com']
    #start_urls = ['http://test.com/']

    def start_requests(self):
        yield scrapy.Request(
            "https://vnexpress.net/dem-hen-cuoi-voi-nguoi-yeu-cu-4519860.html",
            meta=dict(
                playwright=True,
                playwright_include_page=True,
                playwright_page_methods=[
                    PageMethod(
                        "wait_for_selector",
                        "p.full_content",
                        #state="attached",
                    ),
                    ##PageMethod(
                    ##    "wait_for_selector",
                    ##    "p.content_less",
                    ##    state="attached",
                    ##),
                    #PageMethod(
                    #    "is_visible",
                    #    "p.content_less",
                    #),
                    #PageMethod(
                    #    "click",
                    #    "a.view_all_reply",
                    #    #timeout=0,
                    #    #click_count=3,
                    #    #force=True,
                    #    #strict=False,
                    #),
                    ##PageMethod(
                    ##    "is_visible",
                    ##    "a.icon_show_full_comment.continue-reading",
                    ##),
                    ##PageMethod(
                    ##    "click",
                    ##    "a.icon_show_full_comment.continue-reading",
                    ##),
                    ##PageMethod(
                    ##    "click",
                    ##    PageMethod(
                    ##        "get_by_role",
                    ##        "link",
                    ##        name="Đọc tiếp",
                    ##    )
                    ##),
                ],
            ),
        )

    async def parse(self, response):
        for comment in response.css("p.full_content"):
            yield {
                "username": comment.css("b::text").get(),
                "message": comment.css("p::text").get(),
            }
        for comment in response.css("p.content_less"):
            yield {
                "username": comment.css("b::text").get(),
                "message": comment.css("p::text").get(),
            }
