from urllib.parse import quote

#import httpx
import requests
import scrapy

from ..utils import strip_tags


class VnexpressCommentCurlSPIDER(scrapy.Spider):
    name = 'vnexpress_comment_curl'
    allowed_domains = ['vnexpress.net']
    start_urls = [
        "https://vnexpress.net/ba-diem-nong-co-the-dinh-doat-tran-arsenal-liverpool-4520997.html",
    ]

    def parse(self, response):
        title = response.xpath('//h1[@class="title-detail"]/text()').get()

        tagged_content = "".join(response.xpath('//p[@class="Normal"]').getall())
        content = strip_tags(tagged_content)

        # Deal with comments
        # 1. Grab some ids for url request
        objectid = response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objectid"]
        objecttype = response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objecttype"]
        siteid = response.xpath('//meta[@name="tt_site_id"]').attrib["content"]
        categoryid = response.xpath('//meta[@name="tt_category_id"]').attrib["content"]
        percent_code = response.xpath('//title/text()').get()
        percent_code = "+".join(quote(s) for s in percent_code.split(" "))
        curl_url = (
            f"https://usi-saas.vnexpress.net/index/get"
            f"?objectid={objectid}"
            f"&objecttype={objecttype}"
            f"&siteid={siteid}"
            f"&categoryid={categoryid}"
            f"&title={percent_code}"
        )

        r = requests.get(curl_url)
        # TODO: async session
        #with httpx.AsyncClient() as ac:
        #    r = ac.get(curl_url)

        comments = r.json()["data"]["items"]

        # 2. Extract only the useful info from comments
        comments = [strip_tags(d["content"]) for d in comments]

        yield {
            "title": title,
            "content": content,
            "comments": comments,
        }
