from urllib.parse import quote

import requests
import scrapy

from ..utils import strip_tags


class VnexpressSpider(scrapy.Spider):
    name = "vnexpress"
    allowed_domains = ["vnexpress.net"]
    start_urls = ["http://vnexpress.net/"]

    #def start_requests(self):
    #    pass

    def parse(self, response):
        # Don't want home, newlest, all-menu
        unwanted = ("home", "newlest", "podcasts",
                    "video", "all-menu has_transition",)
        #navs = [sel for sel in response.xpath('//nav[@class="main-nav"]/ul/li') if sel.attrib["class"] not in unwanted]
        relpaths = [sel.xpath("./a").attrib["href"] for sel in response.xpath('//nav[@class="main-nav"]/ul/li') if sel.attrib["class"] not in unwanted]
        #relpaths = relpaths[:2]

        #for p in relpaths:
        #    url = response.urljoin(p)
        #    yield scrapy.Request(url, self.parse_theme)
        yield from response.follow_all(relpaths, self.parse_theme)

    def parse_theme(self, response):
        article_urls = [anchor.attrib["href"] for anchor in response.xpath('//h3[@class="title-news"]/a')]
        #self.theme = response.url.split("/")[-1]
        #for url in article_urls:
        #    yield scrapy.Request(url, self.parse_article)
        yield from response.follow_all(article_urls, self.parse_article)

        # TODO: next page
        MAX_PAGE = 500
        page_hint = response.url.split("-")[-1]
        if page_hint[0] == "p" and page_hint[1:].isdigit():
            pass
        else:
            next_urls = [f"{response.url}-p{i}" for i in range(2, MAX_PAGE)]
            yield from response.follow_all(next_urls, self.parse_theme)

    def parse_article(self, response):
        title = response.xpath('//h1[@class="title-detail"]/text()').get()

        #content = "".join(response.xpath('//p[@class="Normal"]/text()').getall())
        tagged_content = "".join(response.xpath('//p[@class="Normal"]').getall())
        content = strip_tags(tagged_content)

        # Prepare for scraping comments (a distinct request to another URL)
        # 1. Grab ingredients for the request URL
        try:
            #objectid = response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objectid"]
            #objecttype = response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objecttype"]
            comment_span = response.xpath('//span[@class="txt_num_comment font_icon"]')
            if len(comment_span) == 0:
                comment_span = response.xpath('//span[@class="number_cmt txt_num_comment num_cmt_detail"]')

            objectid = comment_span.attrib["data-objectid"]
            objecttype = comment_span.attrib["data-objecttype"]


            siteid = response.xpath('//meta[@name="tt_site_id"]').attrib["content"]
            categoryid = response.xpath('//meta[@name="tt_category_id"]').attrib["content"]
            percent_code = response.xpath('//title/text()').get()
            percent_code = "+".join(quote(s) for s in percent_code.split(" "))
        except Exception:
            n_times = 44
            self.log("\n" + "="*n_times)
            self.log(f"{response.url} (see below for origin of err)")
            self.log("\n" + "="*n_times)
            raise

        curl_url = (
            f"https://usi-saas.vnexpress.net/index/get"
            f"?objectid={objectid}"
            f"&objecttype={objecttype}"
            f"&siteid={siteid}"
            f"&categoryid={categoryid}"
            f"&title={percent_code}"
        )

        r = requests.get(curl_url)
        # TODO: async session
        #with httpx.AsyncClient() as ac:
        #    r = ac.get(curl_url)

        comments = r.json()["data"]["items"]

        # 2. Keep only the useful info in comments
        comments = [strip_tags(d["content"]) for d in comments]

        # TODO: include theme/topic
        yield {
            "url": response.url,
            "title": title,
            "content": content,
            "comments": comments,
        }
