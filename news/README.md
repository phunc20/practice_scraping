## Installation
If you find `news/spiders/vnexpress_comment_playwright.py` useful and want to try it, don't forget to
install the following before running that spider:

```bash
$ pip install scrapy_playwright
$ playwright install
```

## The cURL for Requesting Comments
I found that in **Web Developer Tools** in my browser. More precisely, under **Network**
I went through the list of incoming network and eventually found among the requests with type `js`
the one requesting for comments.

The full cURL I copied was as follows.
```bash
curl 'https://usi-saas.vnexpress.net/index/get?offset=0&limit=25&frommobile=0&sort=like&is_onload=1&objectid=4520997&objecttype=1&siteid=1002565&categoryid=1002580&sign=aa220eb3b3c4f18dadec40ba0f04b8ff&tab_active=most_like&cookie_aid=b239i0cge1s1tzoa.1662652616.des&usertype=4&template_type=1&app_mobile_device=0&title=Ba+%C4%91i%E1%BB%83m+n%C3%B3ng+c%C3%B3+th%E1%BB%83+%C4%91%E1%BB%8Bnh+%C4%91o%E1%BA%A1t+tr%E1%BA%ADn+Arsenal+-+Liverpool+-+VnExpress+Th%E1%BB%83+thao'
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:106.0) Gecko/20100101 Firefox/106.0'
-H 'Accept: application/json'
-H 'Accept-Language: en-US,en;q=0.5'
-H 'Accept-Encoding: gzip, deflate, br'
-H 'Origin: https://vnexpress.net'
-H 'Sec-Fetch-Dest: empty'
-H 'Sec-Fetch-Mode: cors'
-H 'Sec-Fetch-Site: same-site'
-H 'Referer: https://vnexpress.net/'
-H 'Connection: keep-alive'
-H 'If-Modified-Since: Sat, 22 Oct 2022 11:04:36 GMT'
-H 'If-None-Match: "MdgQDSTNi8M+DkmHb3M34Q=="'
-H 'TE: trailers'
```

But after I tested, I found that the following shorter version also worked fine:
```bash
curl 'https://usi-saas.vnexpress.net/index/get?objectid=4520997&objecttype=1&siteid=1002565&categoryid=1002580&title=Ba+%C4%91i%E1%BB%83m+n%C3%B3ng+c%C3%B3+th%E1%BB%83+%C4%91%E1%BB%8Bnh+%C4%91o%E1%BA%A1t+tr%E1%BA%ADn+Arsenal+-+Liverpool+-+VnExpress+Th%E1%BB%83+thao'
```
That is, we do not need all those `-H` options.

The constituant key values of the URL could be easily found if one uses `scrapy shell` to investigate the page:

- `objectid=4520997`: `response.url` or `response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objectid"]`
- `objecttype=1`: `response.xpath('//span[@class="txt_num_comment font_icon"]').attrib["data-objecttype"]`
- `siteid=1002565`: `response.xpath('//meta[@name="tt_site_id"]').attrib["content"]`
- `categoryid=1002580`: `response.xpath('//meta[@name="tt_category_id"]').attrib["content"]`
- `title=Ba+%C4%91i%E1%BB%83m+n%C3%B3ng+c%C3%B3+th%E1%BB%83+%C4%91%E1%BB%8Bnh+%C4%91o%E1%BA%A1t+tr%E1%BA%ADn+Arsenal+-+Liverpool+-+VnExpress+Th%E1%BB%83+thao`:
  `quote(response.xpath('//title/text()').get())`

To get usernames to each comment:
```bash
curl 'https://my.vnexpress.net/apifrontend/getusersprofile?myvne_users_id%5B%5D=1002614762&myvne_users_id%5B%5D=1003303056&myvne_users_id%5B%5D=1005772790&myvne_users_id%5B%5D=1006126357&myvne_users_id%5B%5D=1014183616&myvne_users_id%5B%5D=1014195856&myvne_users_id%5B%5D=1014258361&myvne_users_id%5B%5D=1034054882&myvne_users_id%5B%5D=1045093885&myvne_users_id%5B%5D=1049855239&myvne_users_id%5B%5D=1064964675&myvne_users_id%5B%5D=1065083143&myvne_users_id%5B%5D=1066899113&myvne_users_id%5B%5D=1068328822&myvne_users_id%5B%5D=1068839687&myvne_users_id%5B%5D=1071736550&cache=1&callback=InteractionCallback' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:106.0) Gecko/20100101 Firefox/106.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'Sec-Fetch-Dest: script' -H 'Sec-Fetch-Mode: no-cors' -H 'Sec-Fetch-Site: same-site' -H 'Referer: https://vnexpress.net/' -H 'Connection: keep-alive' -H 'Cookie: fosp_uid=b239i0cge1s1tzoa.1662652616.des; fosp_aid=b239i0cge1s1tzoa.1662652616.des; orig_aid=b239i0cge1s1tzoa.1662652616.des; _gcl_au=1.1.1804194410.1662652621; login_system=1; _ga_DQJ7NF9DN2=GS1.1.1666507405.20.1.1666507432.33.0.0; _ga=GA1.2.1729465519.1662652623; _ga_57577CKS2C=GS1.1.1666505063.18.1.1666507432.60.0.0; fpt_uuid=%22d85acdb6-b04f-4e67-82b3-1479a2e469c5%22; ajs_group_id=null; __zi=2000.SSZzejyD3j0kalk-tmODZ2M4_BATGGd2Avo-xezB5z1fZgZzoHHHqst4vVh14rFMRSQg-yyN7T5kmgx_o18Oqm.1; __gads=ID=dbb62ecb57a5416c-22adb0ac13d700db:T=1662652655:S=ALNI_MZrpGcVAZ2TojEklTVvZ1fAeXmHMQ; __gpi=UID=00000999a4bf6140:T=1662652655:RT=1666495286:S=ALNI_MZKfzjU9l4WfmmYr9ITVXYDNn_jpQ; cto_bundle=d9wSml9IamNyRWIlMkJSTnRuTUZ6RzhhVXY2MEVramlvclRpUGp0ODNjMEZzYXVicGRrQXpoSnpBYUplWHY5ZkdMR3dZelZNdXNPVEtmNzZpWm5MNHFwUTQ5YmRpSlI3Z3JqS2hWUXlDdWdSU2tRdmZvUCUyRkpYT0lPVUw3QlRUWHNVY1FvYzl4QXlXOU9LbEJ1MXhBaTRZJTJGb0oweEElM0QlM0Q; _gaexp=GAX1.2.SLafN0PlTmed9g053SaXPQ.19341.1; sw_version=1; g_state={"i_p":1666440481081,"i_l":2}; _gid=GA1.2.578905118.1666353878; fosp_location_zone=1; fosp_loc=29-1-VN; display_cpd=2; device_env=4; _gat_t3=1; _dc_gtm_UA-50285069-28=1; network_speed=0.41792394' -H 'If-Modified-Since: Sun, 23 Oct 2022 13:12:52 GMT' -H 'If-None-Match: 9eb4f4bad585e5adc00fdb59ed3457b3'
```


## Escaping `objectid`
<span class="number_cmt txt_num_comment num_cmt_detail" data-type="comment" data-objecttype="1" data-objectid="4524809"></span>
```bash
curl 'https://usi-saas.vnexpress.net/index/get?offset=0&limit=25&frommobile=0&sort=like&is_onload=1&objectid=4524809&objecttype=1&siteid=1000000&categoryid=1001007&sign=3b911d642e95e55e3f0be23e0d42c407&tab_active=most_like&cookie_aid=b239i0cge1s1tzoa.1662652616.des&usertype=4&template_type=1&app_mobile_device=0&title=C%E1%BB%B1u+b%C3%AD+th%C6%B0%2C+c%E1%BB%B1u+ch%E1%BB%A7+t%E1%BB%8Bch+%C4%90%E1%BB%93ng+Nai+b%E1%BB%8B+b%E1%BA%AFt' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:106.0) Gecko/20100101 Firefox/106.0' -H 'Accept: application/json' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'Origin: https://vnexpress.net' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-site' -H 'Referer: https://vnexpress.net/' -H 'Connection: keep-alive' -H 'TE: trailers'
```

Another escape:
https://vnexpress.net/chuyen-phuot-ve-thanh-xuan-cua-me-ca-si-hoang-bach-4524924.html


## Empty comments
```bash
grep 'comments": \[\]' out.jsonl  | wc -l
```
